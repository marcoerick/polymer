var express=require('express'),
app=express(),
port=process.env.PORT || 3000;

var path = require('path');
var cors=require('cors');
app.use(cors());
app.use(express.static(__dirname + '/build/default'));
//app.use(express.static(__dirname));

app.listen(port);

console.log('App Polymer con fachada de node escuchando en el puerto:'+ port);

app.get ('/',function(req,res){
    res.sendFile("index.html",{root:'.'});
});
